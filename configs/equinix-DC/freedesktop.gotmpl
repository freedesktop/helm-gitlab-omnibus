provider: ""
pagesHosts:
  - docs.mesa3d.org
  - docs.pipewire.org
  - fprint.freedesktop.org
  - geoclue-test.freedesktop.org
  - libbsd-test.freedesktop.org
  - libcacard.freedesktop.org
  - libnice.freedesktop.org
  - libopenraw.freedesktop.org
  - mesa3d.org
  - mesa.freedesktop.org
  - modemmanager.org
  - monado.freedesktop.org
  - networkmanager.dev
  - nouveau.freedesktop.org
  - pages-test.freedesktop.org
  - pages.syncevolution.org
  - panfrost.freedesktop.org
  - piglit.freedesktop.org
  - pipewire.org
  - planet.freedesktop.org
  - poppler.freedesktop.org
  - rarian.freedesktop.org
  - specifications.freedesktop.org
  - syncevolution.org
  - upower.freedesktop.org
  - waffle.freedesktop.org
  - wayland.freedesktop.org
  - www.mesa3d.org
  - www.networkmanager.dev
  - www.pipewire.org
  - xrdesktop.freedesktop.org
  - zeitgeist.freedesktop.org
ingress:
  pagesServerSnippet: |
      set $new_uri "";
      if ($host = "mesa.freedesktop.org") {
        set $new_uri $mesa_uri;
      }
      if ($host = "mesa3d.org") {
        set $new_uri $mesa_uri;
      }
      if ($host = "www.mesa3d.org") {
        set $new_uri $mesa_uri;
      }
      if ($new_uri != "") {
        return 301 $new_uri;
      }

redisStorageClass: rook-ceph-block-ssd

issuer:
  server: https://acme-v02.api.letsencrypt.org/directory
  email: daniel@fooishbar.org
  dns01:
  - dnsZones:
    - freedesktop.org
    nameserver: 131.252.210.177
    name: freedesktop-org-secret
    algorithm: HMACSHA512
    secret: {{ readFile "../../helm-gitlab-secrets/secrets.yaml" | fromYaml | get "freedesktop.issuer.fdo_tsig" }}

registry:
  enabled: true

hetzner_s3:
  host: fsn1.your-objectstorage.com

ceph:
  s3:
    host: rook-ceph-rgw-fdo-s3-dc.rook-ceph.svc
  backups:
    serviceName: rook-ceph-rgw-fdo-backups-dc
  registry:
    serviceName: rook-ceph-rgw-fdo-registry-dc.rook-ceph.svc.dc.clusterset.k3s

post_receive_hook:
  enabled: true
  data:
    kemper_ssh_priv_key: {{ readFile "../../helm-gitlab-secrets/secrets.yaml" | fromYaml | get "freedesktop.kemperSSHPrivKey" | b64enc | quote }}
    kemper_ssh_known_host: {{ readFile "../../helm-gitlab-secrets/secrets.yaml" | fromYaml | get "freedesktop.kemperSSHKnownHost" | b64enc | quote }}
    git_post_receive_mirror_hook: {{ readFile "git_post_receive_hook.sh" | b64enc | quote }}
