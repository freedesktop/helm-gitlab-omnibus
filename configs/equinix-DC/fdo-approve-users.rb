# used for users
denylist = [
  /escort/i,
  /callgirl/i,
  /casino/i,
  /kasino/i,
  /superstore/i,
  /mosquito/i,
  /residence/i,
  /coinbase/i,
  /binance/i,
  /pharmacy/i,
  /Nhà Cái/i,
  /Wintips/i,
  /Win55/i,
  /Win88/i,
  /instagram/i,
  /games/i,
  /gaming/i,
  /soccertips/i,
]

# because of the query we do, the following is actually
# a regex, even if not in '//i'
project_denylist = [
  'huong-dan',  # "guide" in vietnamese
  'crypto.com',
]

# Same, this is a regex
web_url_denylist = [
  'escort',
  'callgirl',
  'livegirl',
  'casino',
  'kasino',
  'superstore',
  'mosquito',
  'residence',
  'coinbase',
  'bitcoin',
  'binance',
  #'games',  # not sure, it might be leggit but most of them are spam
  #'gaming', # not sure, it might be leggit but most of them are spam
  'selleckchem',
  'youtube',
]

# not the best code ever but couldn't figure out how to solve the (irb):212: void value expression
def usermatch(user, filter)
  if user.name.match(filter) or user.username.match(filter) or user.email.gsub(".", "").match(filter)
    then
      return true
    else
      return false
    end
  end

admin = User.find_by_username('userbot')

# first and foremost, trust users who have their accounts created for
# more than 14 days and who requested fork capabilities
User.active.
    where(external: false).
    where('id NOT IN (select distinct(user_id) from user_custom_attributes where key = \'trusted_by\')').
    where('created_at < ?', 14.days.ago).
    each() { | user|
      if not user.trusted?
        puts "trusting " + user.username;
        UserCustomAttribute.set_trusted_by(user: user, trusted_by: admin)
      end
    }

# Trust project bots with an email like
#   project_<id>_bot_<sha>@noreply.gitlab.freedesktop.org.
# This script runs every 10 min so looking at the last 90min is enough.
User.active.
    where(external: true).
    where('id NOT IN (select distinct(user_id) from user_custom_attributes where key = \'trusted_by\')').
    where('created_at > ?', 90.minutes.ago).
    where('email LIKE ?', '%noreply.gitlab.freedesktop.org%').
    each() { | user|
      if not user.trusted? and user.email.match?(/^(project|group)_[0-9]+_bot_[a-f0-9]+@noreply.gitlab.freedesktop.org/)
        puts "trusting bot " + user.username;
        UserCustomAttribute.set_trusted_by(user: user, trusted_by: admin)
      end
    }

# Clean up users who did not verify their email within 24h
User.active.
    where(external: true).
    where('confirmed_at IS NULL').
    where('created_at < ?', 1.days.ago).
    each() { |user|
      puts "delete unverified account #{user.username}"
      Users::DestroyService.new(admin).execute(user)
    }.count()

# clean up users who:
# - have no project
# - are not part of any group
# - created no MR (how could have they?)
# - not participating in any comment
# - not participating in any issue
# - where not active in the past 60 days
# - have no notifcation set up
User.active.
    where('id NOT IN (select distinct(user_id) from project_authorizations)').
    where('id NOT IN (select distinct(author_id) from merge_requests)').
    where('id NOT IN (select distinct(author_id) from notes)').
    where('id NOT IN (select distinct(author_id) from issues)').
    where('id NOT IN (select distinct(user_id) from subscriptions)').
    where("last_activity_on <= ?", 60.days.ago).
    each() { |user|
      if user.notification_settings.count == 0
      then
        puts "delete unused account #{user.username}"
        Users::DestroyService.new(admin).execute(user)
      end
    }

# same here except the user never logged in and was created > 60 days ago
User.active.
    where('id NOT IN (select distinct(user_id) from project_authorizations)').
    where('id NOT IN (select distinct(author_id) from merge_requests)').
    where('id NOT IN (select distinct(author_id) from notes)').
    where('id NOT IN (select distinct(author_id) from issues)').
    where('id NOT IN (select distinct(user_id) from subscriptions)').
    where("last_activity_on IS NULL").
    where("created_at <= ?", 60.days.ago).
    each() { |user|
      if user.notification_settings.count == 0
      then
        puts "delete unused account #{user.username}"
        Users::DestroyService.new(admin).execute(user)
      end
    }

# finally, for existing users, remove the ones that match the denylist
# note that this time we also prune the ones with projects. This is OK as of
# today (June 2022)
User.active.
  where('id NOT IN (select distinct(author_id) from merge_requests)').
  where('id NOT IN (select distinct(author_id) from notes)').
  where('id NOT IN (select distinct(author_id) from issues)').
  where('id NOT IN (select distinct(user_id) from subscriptions)').
  where("created_at >= ?", 60.days.ago).
  each() { |user|
    rejected= false
    denylist.each { |filter|
      next if rejected
      if usermatch(user, filter)
      then
        puts "delete spam account #{user.username}"
        Users::DestroyService.new(admin).execute(user)
        rejected = true
      end
    }
  }

# detect spam projects that matches the project_denylist
deleted_users = []
project_denylist.each { |filter|
  Project.where("name ~* ?", filter).each { |project|
    if project.repository.commit_count == 1 and
       project.users.count <= 1 and
       project.fork_source == nil
    then
      user = project.users.first
      next if deleted_users.include? user
      puts "delete owner of '#{project.name}': #{user.username}"
      deleted_users.push(user)
      Users::DestroyService.new(admin).execute(user)
      next
    end
  }
}

# detect spam website_urls
web_url_denylist.each { |filter|
  User.active.
    where(external: true).
    where('id IN (select user_id from user_details where website_url ~ ?)', filter).
    where('id NOT IN (select distinct(author_id) from merge_requests)').
    where('id NOT IN (select distinct(author_id) from notes)').
    where('id NOT IN (select distinct(author_id) from issues)').
    where('id NOT IN (select distinct(user_id) from subscriptions)').
    each() { |user|
      puts "delete spam account #{user.username}"
      Users::DestroyService.new(admin).execute(user)
    }
}

# External users do not need a profile. Even private profiles's info is public
# so our spammers fill this out for SEO and wiping it just means they fill it in
# again later. So let's continuously wipe any external user's profile.
#
# The only field we allow for external users is the organization
# 
# For now this only runs for users created in the last 60 days, assumption is
# inactive users past that date were purged anyway (see above)
User.active.
  where(external: true).
  where('id IN (select user_id from user_details WHERE website_url != \'\' OR (bio != \'\' AND bio NOT LIKE \'%disabled for spam reasons%\'))').
  where("created_at >= ?", 60.days.ago).
  each() { |user|
    puts "Sanitizing #{user.email}\t#{user.username}"
    user.website_url = ''
    user.bio = 'User profile details are disabled for spam reasons'
    user.public_email = ''
    user.linkedin = ''
    user.twitter = ''
    user.bluesky = ''
    user.skype = ''
    user.location = ''
    user.mastodon = ''
    user.discord = ''
    user.job_title = ''
    user.save
  }

# purge the abuse reports from the annoying "gitlab security bot"
count = 0
AbuseReport.where(reporter_id: User.find_by_username("GitLab-Security-Bot").id).each { |abuse_report|
  AntiAbuse::Event.where(abuse_report: abuse_report.id).destroy_all
  abuse_report.destroy!
  count += 1
}
if count > 0
then
  printf("Removed %d extra abuse reports", count)
end

# and then submit as ham comments from trusted users and remove the matching spamlog entries
count = 0
SpamLog.all.each {|spam_log|
  if spam_log.user.trusted?
  then
    Spam::HamService.new(spam_log).execute
    spam_log.destroy!
    count += 1
  end
}
if count > 0
then
  printf("Submitted %d extra spam false positives", count)
end
