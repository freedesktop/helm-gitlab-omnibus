# This file defines FDO's Gitlab policy using Open Policy Agent (OPA)
# Ref: https://www.openpolicyagent.org/docs/latest/
#
# -- DEBUG --
# You can test the REGO policy with the following JSON in fdo-token.json
#{
#  "input": {
#    "account": "BLAH-FIXME",
#    "action": "s3:ListBucket",
#    "bucket": "git-cache",
#    "conditions": "BLAH-FIXME",
#    "owner": false,
#    "object": "",
#    "claims": {
#      "accessKey": "BLAH-FIXME",
#      "exp": "1593596324",
#      "iat": 1593595394,
#      "iss": "gitlab.freedesktop.org",
#      "job_id": "3378898",
#      "jti": "BLAH-FIXME",
#      "namespace_id": "686",
#      "namespace_path": "bentiss",
#      "nbf": 1593595389,
#      "pipeline_id": "169859",
#      "project_id": "6576",
#      "project_path": "bentiss/test-minio",
#     "ref": "master",
#      "ref_protected": "false",
#      "ref_type": "branch",
#      "sub": "job_3378898",
#      "user_email": "benjamin.tissoires@gmail.com",
#      "user_id": "685",
#      "user_login": "bentiss"
#    }
#  }
#}
#
# Then edit the JSON accordingly and run the following commands:
#   $ opa run fdo-token.json fdo-policy.rego
#   ...
#   > data
# Pay attention to the "allow" boolean value defining the authorisation result.
#
# -- FORMAT --
# Run the following to lint and format:
#  $ opa fmt --write fdo-policy.rego

package oidc.authz

import input.attributes.request.http as http_request
import input.parsed_path

#############
# VARIABLES #
#############
default allow := false

read_operations := {
	"GET",
	"HEAD",
}

write_only_operations := {
	"POST",
	"PUT",
	"DELETE",
}

write_operations := read_operations | write_only_operations

gitlab_jwks := "https://gitlab.freedesktop.org/oauth/discovery/keys"

# For non owners (JWT auth)
read_repos := {
	"artifacts",
	"git-cache",
	"gst-rootfs",
	"mesa-lava",
	"mesa-rootfs",
	"mesa-tracie-public",
	"mesa-tracie-results",
}

tracie_private_users := {
	"marge-bot",
	"tomeu",
	"robclark",
	"anholt",
	"flto",
	"cwabbott0",
	"Danil",
	"dh",
	"gallo",
	"hakzsam",
	"kwg",
	"llanderwelin",
	"valentine",
	"vigneshraman",
	"zmike",
}

##########
# POLICY #
##########
# Decode the JWT token
token := parsed if {
	authorization_type == "Bearer"
	jwt := split(http_request.headers.authorization, " ")[1]
	[_, decoded, _] := io.jwt.decode(jwt)
	parsed := object.union(decoded, {"project_name": trim_prefix(decoded.project_path, sprintf("%v/", [decoded.namespace_path]))})
}

# Verify JWT with Gitlab JWKS
jwks_request(url) := http.send({
	"url": url,
	"method": "GET",
	"force_cache": true,
	"force_cache_duration_seconds": 3600,
})

jwks := jwks_request(gitlab_jwks).raw_body
is_jwt_verified := io.jwt.verify_rs256(token, jwks)

# allow if {
# 	is_jwt_verified
# }

authorization_type := parsed if {
	parsed := split(http_request.headers.authorization, " ")[0]
}

# Keep READ access for everyone in $read_repos
allow if {
	read_repos[parsed_path[0]]
	parsed_path[1] != "" # prevent directory listing
	read_operations[http_request.method]
}

# Git cache
allow if {
	parsed_path[0] == "git-cache"
	parsed_path[1] == token.namespace_path
	parsed_path[2] == token.project_name
	parsed_path[3] == concat(".", [token.project_name, "tar.gz"])
	is_jwt_verified
	write_operations[http_request.method]
}

# Gitlab artifacts
allow if {
	pps = concat("/", parsed_path)
	ins = concat("/", ["artifacts", token.project_path, token.pipeline_id])
	startswith(pps, ins)
	is_jwt_verified
	write_operations[http_request.method]
}

# Mesa CI: LAVA jobs allow writing a rootfs/kernel/etc from jobs, then reading them back from anywhere
allow if {
	pps = concat("/", parsed_path)
	ins = concat("/", ["mesa-lava", token.project_path])
	startswith(pps, ins)
	is_jwt_verified
	write_operations[http_request.method]
}

# Mesa CI: kernel/etc jobs allow writing rootfs components from jobs, then reading them back from anywhere
allow if {
	pps = concat("/", parsed_path)
	ins = concat("/", ["mesa-rootfs", token.project_path])
	startswith(pps, ins)
	is_jwt_verified
	write_operations[http_request.method]
}

# Mesa CI: Tracie trace files can be written from traces-db jobs, and read from anywhere
allow if {
	parsed_path[0] == "mesa-tracie-public"
	token.project_path == "gfx-ci/tracie/traces-db"
	is_jwt_verified
	write_operations[http_request.method]
}

# Mesa CI: Tracie private trace files can be written from traces-db-private jobs
allow if {
	parsed_path[0] == "mesa-tracie-private"
	token.project_path == "gfx-ci/tracie/traces-db-private"
	is_jwt_verified
	write_only_operations[http_request.method]
}

# Mesa CI: Tracie private trace files can be read from jobs triggered by an authorized user
allow if {
	parsed_path[0] == "mesa-tracie-private"
	tracie_private_users[token.user_login]
	is_jwt_verified
	read_operations[http_request.method]
}

# Mesa CI: Tracie can be written from mesa/mesa jobs, and read from anywhere
allow if {
	parsed_path[0] == "mesa-tracie-results"
	token.project_path == "mesa/mesa"
	is_jwt_verified
	write_operations[http_request.method]
}

# GStreamer CI: kernel/etc jobs allow writing rootfs components from jobs, then reading them back from anywhere
allow if {
	pps = concat("/", parsed_path)
	ins = concat("/", ["gst-rootfs", token.project_path])
	startswith(pps, ins)
	is_jwt_verified
	write_operations[http_request.method]
}
