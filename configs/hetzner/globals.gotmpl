global:
  hosts:
    domain: freedesktop.org
    ssh: ssh.gitlab.freedesktop.org
  appConfig:
    omniauth:
      enabled: true
      allowSingleSignOn: true
      blockAutoCreatedUsers: false
      providers:
        - secret: freedesktop-prod-omniauth-providers
          key: google
        - secret: freedesktop-prod-omniauth-providers
          key: gitlab
        - secret: freedesktop-prod-omniauth-providers
          key: github
    object_store:
      enabled: true
      proxy_download: true
      storage_options: {}
      connection:
        secret: freedesktop-prod-hetzner-s3-key
        key: connection
    backups:
      # Disabled in the chart due to freedesktop/helm-gitlab-omnibus@35a9e9326ba7
      proxy_download: false
      bucket: 'fdo-gitlab-data-backup'
      tmpBucket: 'fdo-gitlab-tmp-backup-storage'
    uploads:
      bucket: 'fdo-gitlab-uploads'
      proxy_download: false
    artifacts:
      bucket: 'fdo-gitlab-artifacts'
      proxy_download: false
    lfs:
      bucket: 'fdo-gitlab-lfs'
      proxy_download: false
    packages:
      bucket: 'fdo-gitlab-packages'
      proxy_download: false
    sidekiq:
      routingRules:
      - ["name=ci_destroy_old_pipelines", "ci-destroy-old-pipelines"]
      - ["*", "default"]
  email:
    from: 'gitlab@gitlab.freedesktop.org'
  ingress:
    class: gitlab-nginx
    configureCertmanager: false
    tls:
      secretName: gitlab-tls
    annotations:
      cert-manager.io/cluster-issuer: "freedesktop-prod-gitlab-issuer"
      nginx.ingress.kubernetes.io/configuration-snippet: |
        set $new_uri "";
        if ($host = "gitlab.freedesktop.org") {
          set $new_uri $gitlab_uri;
        }
        if ($new_uri != "") {
          return 301 $new_uri;
        }

  smtp:
    enabled:  true
    address: 'gabe.freedesktop.org'
    port: 5878
    user_name: 'gitlab@gitlab.freedesktop.org'
    domain: 'gitlab.freedesktop.org'
    authentication: 'login'
    starttls_auto: true
    tls: false
    openssl_verify_mode: 'peer'
    password:
      secret: freedesktop-prod-smtp-secret
  gitaly:
    enabled: true
    service:
      name:
        gitaly-dc
    internal:
      names:
        - gitaly-0
        - gitaly-1
        - gitaly-2
        - gitaly-3
    external:
      - name: default
        #hostname: gitlab-prod-gitaly-0.gitlab-prod-gitaly.gitlab.svc.cluster.local
        hostname: gitaly-0.gitlab.svc
  praefect:
    enabled: false
  omnibus:
    omnibusCpu: 6
    omnibusMemory: 18Gi
  operator:
    enabled: false
  pages:
    enabled: true
    host: pages.freedesktop.org
    port: 443
    https: true
    externalHttp:
    - 1.2.3.4
    artifactsServer: true
    objectStore:
      enabled: true
      bucket: fdo-gitlab-pages
      connection:
        secret: freedesktop-prod-hetzner-s3-key
        key: connection
  psql:
    host: 172.30.1.254
    port: 6432
    load_balancing:
      hosts:
      - 172.30.1.5
      - 172.30.1.6
      - 172.30.1.2
    password:
      secret: gitlab-prod-postgresql-password
      key: postgresql-password
    # uncomment the following once db is split
    database: null
    ci:
      database: gitlab_production_ci
    main:
      database: gitlab_production
  extraEnv:
    GITLAB_ALLOW_SEPARATE_CI_DATABASE: 1
  redis:
    host: gitlab-prod-redis-master.gitlab.svc
  spamcheck:
    enabled: true
  kas:
    enabled: false
